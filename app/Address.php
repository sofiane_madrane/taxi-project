<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    protected $fillable = ['name',
                            'number','box','postcode',
                            'city','country',
                            'information'];

    /**
     * Get the driver's address.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver() {
        return $this->belongsTo('App\Driver');
    }

    /**
     * Get the passenger's address.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function passenger() {
        return $this->belongsTo('App\Passenger');
    }

    /**
     * Get the passenger's pickup address.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function passenger_pickup(){
        return $this->belongsTo('App\Passenger','pick_up_address');
    }

    /**
     * Get the departure for a ride.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ride_from() {
        return $this->belongsTo('App\Ride','from_address_id');
    }

    /**
     * Get the destination for a ride.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ride_to() {
        return $this->belongsTo('App\Ride','to_address_id');
    }


}
