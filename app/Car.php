<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    //
    protected $fillable = ['mark',
                            'model','place',
                            'sn','insurance',
                            'registration_certificate'];

    public function drivers(){
        return $this->hasMany('App\Driver');
    }
}
