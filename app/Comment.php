<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //

    protected $fillable = ['comment','driver_id','passenger_id'];
    /**
     * Get the passenger record associated with the evaluation.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public  function passenger(){
        return $this->belongsTo('App\Passenger');
    }

    /**
     * Get the driver record associated with the evaluation.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver(){
        return $this->belongsTo('App\Driver');
    }
}
