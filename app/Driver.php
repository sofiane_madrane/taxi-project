<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    //

    protected $fillable = ['fname','sname','email',
                            'phone_number','birthday',
                            'driver_license','id_card','pro_license',
                            'car_id','address_id'];

    /**
     * Get the adrress record associated with the user.
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function address(){
        return $this->hasOne('App\Address');
    }

    /**
     * Get the car record associated with the driver.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function car(){
        return $this->belongsTo('App\Car');
    }

    /**
     *  Get the evaluation record associated with the driver.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evaluations(){
        return $this->hasMany('App\Evaluation');
    }
}
