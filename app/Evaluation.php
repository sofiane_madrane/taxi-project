<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    //
    protected $fillable = ['kindness','respectful','serious',
                            'conduct','driver_id','passenger_id'];
    /**
     * Get the driver record associated with the evaluation.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver(){
        return $this->belongsTo('App\Driver');
    }

    /**
     * Get the passenger record associated with the evaluation.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public  function passenger(){
        return $this->belongsTo('App\Passenger');
    }
}
