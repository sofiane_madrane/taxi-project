<?php

namespace App\Http\Controllers;

use App\Address;
use App\Http\Requests\RideRequest;
use App\Ride;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RidesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $rides = Ride::get();

        return view('ad_list_view',compact('rides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('add_ride');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RideRequest $request)
    {
        //dd($request->all());

        $ride = new Ride();
        $ride->from_address_id = 2;
        $ride->to_address_id = 5;
        $ride->date_time = $request->get('date_time');
        $dt = strtotime(str_replace('/','-',$request->get('date_time')));
        $ride->date_time = date('Y-m-d H:i:s', $dt) ;
        $ride->price = $request->get('price');
        $ride->number_places = $request->get('number_places');
        $ride->pet = $request->has('pet') ? true : false;
        $ride->get_client = $request->has('get_client') ? true : false;
        $ride->save();

        return redirect($ride->path());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ride = Ride::findOrFail($id);
        return view('single',compact('ride'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
