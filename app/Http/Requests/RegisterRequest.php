<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ];
        //'required|string|min:6|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/'
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Require first name',
            'last_name.required' => 'Require last name',
            'first_name.max' => 'The first name must have less 255 characters' ,
            'last_name.max' => 'The last name must have less 255 characters' ,
            'email.required' => 'Require email address',
            'email.email' => '|email',
            'password.required' => 'Require password',
            'password.min' =>'The password does not contains less 6 characters'
        ];
    }
}
