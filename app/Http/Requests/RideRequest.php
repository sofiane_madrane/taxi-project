<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => 'required',
            'to' => 'required',
            'date_time'=>'required|date_format:"d/m/Y H:i"|after:today' ,
            'price'=> 'required|numeric|gt:0',
            'number_places'=> 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            "from.required" => "Select a pickup-address",
            "to.required" => "Select a destination address",
            "date_time.required" => "Entrez une date",
            "date_time.date" => "Il faut une date correct",
            "date_time.after" => "Ne rentrez pas une date antérieur à celle d'aujourd'hui",
            "price.required" => "Enter a price",
            "price.numeric" => "The price must be a numeric value",
            "price.gt" => "The price must be greater that 0",
            "number_places.required" => "Enter a number of places",
            "number_places.numeric" => "The number of places must be a numeric value"
        ];
    }
}
