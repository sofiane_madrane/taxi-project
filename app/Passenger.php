<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passenger extends Model
{
    //

    protected $fillable = ['fname','sname','email',
                            'phone_number','birthday',
                            'cardpay','address_id','pick_up_address'];

    /**
     *  Get the address record associated with the passenger.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses() {
        return $this->hasMany('App\Address');
    }

    /**
     *  Get the comment record associated with the passenger.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments() {
        return $this->hasMany('App\Comment');
    }

    /**
     *  Get the evaluation record associated with the passenger.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evaluations(){
        return $this->hasMany('App\Evaluation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reservation(){
        return $this->belongsTo('App\Reservation');
    }

}
