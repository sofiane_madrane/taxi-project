<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ride extends Model
{
    protected $fillable = ['from_address_id', 'to_address_id','date_time','price','number_places','pet','get_client'];


    public  function path(){
        return "/ride/$this->id";
    }


    /**
    * Get the adrress record associated with the ride.
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function address(){
        return $this->hasOne('App\Address');
    }

    /**
     * Get the reservations records associated with the ride.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservations(){
        return $this->hasMany('App\Reservation');
    }
}
