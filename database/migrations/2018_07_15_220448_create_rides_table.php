<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rides', function (Blueprint $table) {
            $table->increments('id');

            //FK address
            $table->integer('from_address_id')->unsigned()->index();
            $table->foreign('from_address_id')->references('id')->on('addresses');
            $table->integer('to_address_id')->unsigned()->index();
            $table->foreign('to_address_id')->references('id')->on('addresses');


            $table->dateTime('date_time');
            $table->double('price');
            $table->integer('number_places');
            $table->boolean('pet')->nullable()->default('0');
            $table->boolean('get_client')->nullable()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rides');
    }
}
