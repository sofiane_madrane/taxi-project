<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passengers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('sname');
            $table->string('email')->unique()->index();
            $table->string('phone_number')->unique();
            $table->date('birthday');
            $table->string('cardpay')->unique();
            $table->boolean('deleted')->default(false);
            //FK address
            $table->integer('address_id')->unsigned()->index();
            $table->foreign('address_id')
                    ->references('id')
                    ->on('addresses')
                    ->onDelete('cascade');
            //FK address pick-up
            $table->integer('pick_up_address')->unsigned()->index()->nullable();
            $table->foreign('pick_up_address')
                  ->references('id')
                  ->on('addresses')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passengers');
    }
}
