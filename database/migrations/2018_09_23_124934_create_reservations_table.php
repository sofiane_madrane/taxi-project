<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->primary(['driver_id','passenger_id','ride_id']);
            //FK driver
            $table->integer('driver_id')->unsigned()->index();
            $table->foreign('driver_id')->references('id')->on('drivers');
            //FK passenger
            $table->integer('passenger_id')->unsigned()->index();
            $table->foreign('passenger_id')->references('id')->on('passengers');
            // FK ride
            $table->integer('ride_id')->unsigned()->index();
            $table->foreign('ride_id')->references('id')->on('rides')->onDelete('cascade');
            //todo faire les methodes relationnel dans le model et le reverse



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
