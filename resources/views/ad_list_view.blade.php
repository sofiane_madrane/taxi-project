@extends('layouts.template')
@section('content')
    <!--include('layouts.page_search')-->

    <section class="section-sm">
        <div class="container">

            <!-- mettre un if au caus où si tu mets la page_search
            <div class="row">
                <div class="col-md-12">
                    <div class="search-result bg-gray">
                        <h2>Results For "Electronics"</h2>
                        <p>123 Results on 12 December, 2017</p>
                    </div>
                </div>
            </div>
            -->

            <div class="row justify-content-center">
                <div class="col-lg-9 col-md-8 align-content-center">

                    <div class="category-search-filter">
                        <div class="row">
                            <div class="col-md-6">
                                <strong>Short</strong>
                                <select>
                                    <option>Most Recent</option>
                                    <option value="1">Most Popular</option>
                                    <option value="2">Lowest Price</option>
                                    <option value="4">Highest Price</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <!-- ad listing list  -->
                    @foreach($rides as $ride)
                    <div class="ad-listing-list mt-20">
                        <div class="row p-lg-3 p-sm-5 p-9">
                            <div class="col-lg-4 align-self-center">
                                <a href="{{url($ride->path())}}">
                                    <img src="/images/products/products-1.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-6 col-md-10">

                                        <div class="ad-listing-content">
                                            <div>
                                                <a href="{{url($ride->path())}}" class="font-weight-bold">11inch Macbook Air</a>
                                            </div>
                                            <ul class="list-inline mt-2 mb-3">
                                                <li class="list-inline-item"> <i class="fa fa-folder-open-o"></i> </li>
                                                <li class="list-inline-item"><i class="fa fa-calendar"></i>{{$ride->date_time}}</li>
                                            </ul>
                                            <p class="pr-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, aliquam!</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 align-self-center">
                                        <div class="product-ratings float-lg-right pb-3">
                                            <ul class="list-inline">
                                                <li class="list-inline-item selected"><i class="fa fa-star"></i></li>
                                                <li class="list-inline-item selected"><i class="fa fa-star"></i></li>
                                                <li class="list-inline-item selected"><i class="fa fa-star"></i></li>
                                                <li class="list-inline-item selected"><i class="fa fa-star"></i></li>
                                                <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!-- ad listing list  -->
                </div>
            </div>

        </div>
    </section>

@endsection