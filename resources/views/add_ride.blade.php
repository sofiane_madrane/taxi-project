@extends('layouts.template')
@section('content')

    <!--==================================
    =            User Profile            =
    ===================================-->

    <section class="user-profile section">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1 col-lg-3 offset-lg-0">
                    <div class="sidebar">
                        <!-- User Widget -->
                        <div class="widget user">
                            <!-- User Image -->
                            <div class="image d-flex justify-content-center">
                                <img src="/images/user/user-thumb.jpg" alt="" class="">
                            </div>
                            <!-- User Name -->
                            <h5 class="text-center">Samanta Doe</h5>
                        </div>
                        <!-- Dashboard Links -->
                        <div class="widget dashboard-links">
                            <ul>
                                <li><a class="my-1 d-inline-block" href="">Savings Dashboard</a></li>
                                <li><a class="my-1 d-inline-block" href="">Saved Offer <span>(5)</span></a></li>
                                <li><a class="my-1 d-inline-block" href="">Favourite Stores <span>(3)</span></a></li>
                                <li><a class="my-1 d-inline-block" href="">Recommended</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 offset-md-1 col-lg-9 offset-lg-0">
                    <!-- Edit Profile Welcome Text -->
                    <div class="widget welcome-message">
                        <h2>Add a Trip</h2>
                        <p>Submit your services ! Set up your trip and put it online to attract customers and make money !</p>
                    </div>

                    <div class="widget personal-info">

                        <form role="form"  method="post" action="{{url('ride')}}">
                            <h3 class="widget-header user">Pick up & Destination</h3>
                            {{csrf_field()}}
                            <!-- First Name -->
                            <div class="form-group">
                                <label for="first-name">From</label>
                                <input name="from" type="text"
                                       class="form-control {!! $errors->has('from') ? 'is-invalid' :'' !!}"
                                       value="{{old('from')}}" >
                                {!! $errors->first('from', '<p class="invalid-feedback">:message</p>') !!}
                            </div>



                            <!-- Last Name -->
                            <div class="form-group">
                                <label for="last-name">To</label>
                                <input name="to" type="text"
                                       class="form-control {!! $errors->has('to') ? 'is-invalid' :'' !!}"
                                       value="{{old('to')}}" >
                                {!! $errors->first('to', '<p class="invalid-feedback">:message</p>') !!}
                            </div>

                            <div class="form-group date" id="datetimepicker2" data-target-input="nearest">
                                <input name="date_time"
                                       type="text"
                                       class="form-control datetimepicker-input {!! $errors->has('date_time') ? 'is-invalid' :'' !!} "
                                       data-target="#datetimepicker2"
                                       value="{{old('date_time')}}"/>
                                <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                                {!! $errors->first('date_time', '<p class="invalid-feedback">:message</p>') !!}
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker2').datetimepicker('locale', 'fr')
                                });
                            </script>



                            <h3 class="widget-header user">Trip's Detail</h3>
                            <div class="form-check">
                                <input name="get_client[]" class="form-check-input" type="checkbox"  id="get_client" value="{{old('get_client')}}"/>
                                <label class="form-check-label" for="recover_clients">
                                    Recover clients
                                </label>
                            </div>
                            <!-- Checkbox -->
                            <div class="form-check">
                                <input name="pet[]" class="form-check-input" type="checkbox" id="pet" value="{{old('pet')}}"/>
                                <label class="form-check-label" for="pet">
                                    Pet
                                </label>
                            </div>

                            <div>
                                <select name="number_places" class="custom-select form-group {!! $errors->has('number_places') ? 'is-invalid' :'' !!} ">
                                    <option selected value="{{old('number_places')}}" >number of places</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                </select>
                                {!! $errors->first('number_places', '<p class="invalid-feedback">:message</p>') !!}
                            </div>
                            <!-- Price-->
                            <div class="form-group">
                                <label for="current-price">Price</label>
                                <input name="price" type="number" class="form-control {!! $errors->has('price') ? 'is-invalid' :'' !!} "
                                       id="current-price" value="{{old('price')}}">
                                {!! $errors->first('price', '<p class="invalid-feedback">:message</p>') !!}
                            </div>



                            <button type="submit" class="btn btn-transparent " >Add a Trip</button>
                        </form>

                    </div>

                </div>


            </div>
        </div>
    </section>

@endsection