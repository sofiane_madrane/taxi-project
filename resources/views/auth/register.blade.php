@extends('layouts.template')
@php $title = "Sign up"; @endphp
@section('content')

    <section class="login py-5 border-top-1">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-8 align-item-center">
                    <div class="border border">
                        <h3 class="bg-gray p-4">Register Now</h3>
                        <form action="{{ route('register') }}" method="POST">
                            {{csrf_field()}}
                            <fieldset class="p-4">

                                <div class="form-group ">
                                    <input type="text" name="first_name" placeholder="Your First name" class="border p-3 w-100 my-2 form-control {!! $errors->has('first_name') ? 'is-invalid' :'' !!}" value="{{old('first_name')}}">
                                    {!! $errors->first('first_name', '<p class="invalid-feedback">:message</p>') !!}

                                </div>

                                <div class="form-group">
                                    <input type="text" name="last_name" placeholder="Your Last name" class="border p-3 w-100 my-2 form-control {!! $errors->has('last_name') ? 'is-invalid' :'' !!}" value="{{old('last_name')}}">
                                    {!! $errors->first('last_name', '<p class="invalid-feedback">:message</p>') !!}
                                </div>

                                <div class="form-group">
                                    <input type="email" name="email" placeholder="Your email address" class="border p-3 w-100 my-2 form-control {!! $errors->has('email') ? 'is-invalid' :'' !!}" value="{{old('email')}}">
                                    {!! $errors->first('email', '<p class="invalid-feedback">:message</p>') !!}
                                </div>


                                <div class="form-group">
                                    <input type="password" name="password" placeholder="Your Password" class="border p-3 w-100 my-2 form-control {!! $errors->has('password') ? 'is-invalid' :'' !!}" value="{{old('password')}}">
                                    {!! $errors->first('password', '<p class="invalid-feedback">:message</p>') !!}
                                </div>

                                <div class="form-group">
                                    <input type="password" name="password_confirmation" placeholder="Confirm Password*" class="border p-3 w-100 my-2 form-control {!! $errors->has('password') ? 'is-invalid' :'' !!}" value="{{old('password_confirmation')}}">
                                    {!! $errors->first('password_confirmation', '<p class="invalid-feedback">:message</p>') !!}
                                </div>


                                <div class="loggedin-forgot d-inline-flex my-3">
                                    <input type="checkbox" id="registering" class="mt-1">
                                    <label for="registering" class="px-2">By registering, you accept our <a class="text-primary font-weight-bold" href="{{url('terms-condition')}}">Terms & Conditions</a></label>
                                </div>

                                <button type="submit" class="d-block py-3 px-4 bg-primary text-white border-0 rounded font-weight-bold">Register Now</button>

                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection