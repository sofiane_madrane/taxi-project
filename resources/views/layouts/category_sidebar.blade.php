<div class="col-lg-3 col-md-4">
    <div class="category-sidebar">
        <div class="widget category-list">
            <h4 class="widget-header">Vehicle</h4>
            <ul class="category-list">
                <li><a href="category.html">Car <span>93</span></a></li>
                <li><a href="category.html">Van <span>233</span></a></li>
            </ul>
        </div>

        <div class="widget category-list">
            <h4 class="widget-header">Nearby</h4>
            <ul class="category-list">
                <li><a href="category.html">Brussels Airport <span>93</span></a></li>
                <li><a href="category.html">Brussels South Airport Charleroi <span>233</span></a></li>
            </ul>
        </div>

        <div class="widget filter">
            <h4 class="widget-header">Show Produts</h4>
            <select>
                <option>Popularity</option>
                <option value="1">Top rated</option>
                <option value="2">Lowest Price</option>
                <option value="4">Highest Price</option>
            </select>
        </div>

        <div class="widget price-range w-100">
            <h4 class="widget-header">Price Range</h4>
            <div class="block">
                <input class="range-track w-100" type="text" data-slider-min="0" data-slider-max="5000" data-slider-step="5"
                       data-slider-value="[0,5000]">
                <div class="d-flex justify-content-between mt-2">
                    <span class="value">10€ - 120€</span>
                </div>
            </div>
        </div>

        <div class="widget product-shorting">
            <h4 class="widget-header">By Condition</h4>
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" value="">
                    Brand New
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" value="">
                    Almost New
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" value="">
                    Gently New
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" value="">
                    Havely New
                </label>
            </div>
        </div>

    </div>
</div>