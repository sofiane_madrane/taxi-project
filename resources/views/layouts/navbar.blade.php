
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light navigation">
                    <a class="navbar-brand" href="{{url('/')}}">
                        <img src="/images/logo_voyager.png" alt="" width="100">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <ul class="navbar-nav ml-auto mt-10">

                        <li class="nav-item active">
                            <a class="nav-link" href="{{url('/')}}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="{{url('ride/new')}}"><i class="fa fa-plus-circle"></i> Suggest a trip</a>
                        </li>
                        @guest()
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('login')}}">Log in</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white signup-button" href="{{route('showRegistrationForm')}}">Sign up</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href=" ">My account</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('logout')}}">Log out</a>
                            </li>
                        @endguest

                    </ul>

                </nav>
            </div>
        </div>
    </div>
</section>