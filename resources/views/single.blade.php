@extends('layouts.template')

@section('content')

    <!--page-search
    include('layouts.page_search')-->

    <!--===================================
    =            Store Section            =
    ====================================-->
    <section class="section bg-gray">
        <!-- Container Start -->
        <div class="container">
            <div class="row">
                <!-- Left sidebar -->
                <div class="col-md-8">
                    <div class="product-details">
                        <h1 class="product-title">Brussels to Brussels South Airport Charleroi</h1>
                        <h3> {{$ride->date_time}}</h3>
                        <div class="product-meta">
                            <ul class="list-inline">
                                <li class="list-inline-item"> By <a href="">Andrew</a></li>
                                <li class="list-inline-item"><i class="fa fa-folder-open-o"></i> Vehicle <a href="">Car</a></li>
                                <li class="list-inline-item"><i class="fa fa-location-arrow"></i> Location <a href="">Brussels</a></li>
                                <li class="list-inline-item"><i class="fa fa-user-o"></i> Users <a href="">{{$ride->number_places}}</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="single-listing-content">


                        <ul class="list-group list-group-flush overview-content">
                            <li class="list-group-item">2 remaining places</li>
                            <li class="list-group-item">Smoking is not allowed in the car.</li>
                            <li class="list-group-item">No pets in the car.</li>
                            <li class="list-group-item">Mercedez-Benz E-Klass</li>
                        </ul>

                        <div class="listing-reviews-area mt-100" id="review">
                            <h3> Review</h3>
                            <div class="single-review-area">

                                <div class="product-review">
                                    <div class="media">
                                        <!-- Avater -->
                                        <img src="/images/user/user-thumb.jpg" alt="avater">
                                        <div class="media-body">
                                            <!-- Ratings -->
                                            <div class="ratings">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item">
                                                        <i class="fa fa-star"></i>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <i class="fa fa-star"></i>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <i class="fa fa-star"></i>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <i class="fa fa-star"></i>
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <i class="fa fa-star"></i>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="name">
                                                <h5>Jessica Brown</h5>
                                            </div>
                                            <div class="date">
                                                <p>Mar 20, 2018</p>
                                            </div>
                                            <div class="review-comment">
                                                <p>
                                                    Good Driver.
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="col-md-4">
                    <div class="sidebar">
                        <div class="widget price text-center">
                            <h4>Price</h4>
                            <p>{{$ride->price}}€</p>
                            <h5>per place</h5>
                        </div>
                        <!-- User Profile widget -->
                        <div class="widget user text-center">
                            <img class="rounded-circle img-fluid mb-5 px-5" src="images/user/user-thumb.jpg" alt="">
                            <h4><a href="">Jonathon Andrew</a></h4>
                            <p class="member-time">Member Since Jun 27, 2017</p>
                            <a href="">See all ads</a>
                            <ul class="list-inline mt-20">
                                <li class="list-inline-item"><a href="" class="btn btn-contact d-inline-block  btn-primary px-lg-5 my-1 px-md-3">Contact</a></li>
                            </ul>
                        </div>
                        <!-- Rate Widget -->
                        <div class="widget rate">
                            <!-- Heading -->
                            <h5 class="widget-header text-center">What would you rate
                                <br>
                                this product</h5>
                            <!-- Rate -->
                            <div class="starrr"></div>
                        </div>

                        <!-- Coupon Widget -->
                        <div class="widget coupon text-center">
                            <!-- Coupon description -->
                            <p>Are you satisfied with this driver? Share it with your fellow users.
                            </p>
                            <!-- Submii button -->
                            <a href="" class="btn btn-transparent-white">Submit Listing</a>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <!-- Container End -->
    </section>

@endsection