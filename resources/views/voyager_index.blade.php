@extends('layouts.template')
@section('content')

    <!--===============================
    =            Hero Area            =
    ================================-->

    <section class="hero-area bg-1 text-center overly">
        <!-- Container Start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Header Contetnt -->
                    <div class="content-block">
                        <h1>Reserve & Share Cab Near You </h1>
                        <p>Join the millions of people  who share a cab  <br> everyday to go to the same place around the world</p>

                    </div>
                    <!-- Advance Search -->
                    <div class="advance-search">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-12 col-md-12 align-content-center">
                                    <form>
                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                                <input type="text" class="form-control my-2 my-lg-1" id="inputFrom" placeholder="From">
                                            </div>

                                            <div class="form-group col-md-3">
                                                <input type="text" class="form-control my-2 my-lg-1" id="inputTo" placeholder="To">
                                            </div>

                                            <div class="form-group col-md-4 align-self-center">
                                                <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2"/>
                                                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <script type="text/javascript">
                                                $(function () {
                                                    $('#datetimepicker2').datetimepicker('locale', 'fr')
                                                });
                                            </script>


                                            <div class="form-group col-md-2 align-self-center">
                                                <button type="submit" class="btn btn-primary">Search Now</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container End -->



        <!--====================================
        =            Call to Action            =
        =====================================-->

        <section class="call-to-action overly  section-sm">
            <!-- Container Start -->
            <div class="container">
                <div class="row justify-content-md-center text-center">
                    <div class="col-md-8">
                        <div class="content-holder">
                            <h2>Curious to know what the drivers propose?</h2>
                            <h3>Pssst. It's this way...</h3>
                            <ul class="list-inline mt-30">
                                <li class="list-inline-item"><a class="btn btn-primary" href="{{'ride/listing'}}">Browser Listing</a></li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Container End -->
        </section>


    </section>

    <!--==========================================
      =           Driver propose service           =
      ===========================================-->

    <section class="section">
        <!-- Container Start -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-6">
                    <img src="images/illustrations/driver1.jpg" class="img-fluid" alt="">
                </div>

                <div class="col-6 ">
                    <h2>Are you a driver? Share your service!</h2>
                    <p>Together, let millions of people move!</p>
                    <ul class="list-inline mt-30 align-center">
                        <li class="list-inline-item"><a class="btn btn-primary" href="{{url('ride/new')}}">Suggest a trip</a></li>
                    </ul>
                </div>
            </div>

        </div>
        <!-- Container End -->
    </section>







@endsection