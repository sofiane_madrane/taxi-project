<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/','VoyagerController@index');
/*
Route::get('login', function () {
    return view('login');
});

Route::get('register', function () {
    return view('register');
});

Route::get('dashboard', function () {
    return view('dashboard');
});

Route::get('dashboard-my-ads', function () {
    return view('dashboard_my_ads');
});

Route::get('dashboard-favourite-ads', function () {
    return view('dashboard_favourite_ads');
});

Route::get('dashboard-pending-ads', function () {
    return view('dashboard_pending_ads');
});

Route::get('dashboard-archived-ads', function () {
    return view('dashboard_archived_ads');
});


Route::get('user-profile', function () {
    return view('user_profile');
});

Route::get('404', function () {
    return view('404');
});

Route::get('package', function () {
    return view('package');
});

Route::get('single', function () {
    return view('single');
});

Route::get('store', function () {
    return view('store');
});


Route::get('single-blog', function () {
    return view('single_blog');
});

Route::get('blog', function () {
    return view('blog');
});

Route::get('category', function () {
    return view('category');
});

Route::get('add-ride', function () {
    return view('add_ride');
});

Route::get('terms-condition', function () {
    return view('terms_condition');
});

Route::get('ad-list-view', function () {
    return view('ad_list_view');
});
*/
Route::get('about-us', function () {
    return view('about_us');
});

Route::get('contact-us', function () {
    return view('contact_us');
});

Route::group(['prefix' => 'ride'],function(){
    //Route::resource('ride','RidesController');
    Route::get('new', 'RidesController@create');
    Route::get('listing','RidesController@index');
    Route::get('/{slug}','RidesController@show');
    Route::post('/','RidesController@store');
});

Route::group(['namespace' => 'Auth'],function(){
    Route::get('register', 'RegisterController@showRegistrationForm')->name('showRegistrationForm');
    Route::post('register','RegisterController@store')->name('register');
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@store');
    Route::get('logout', 'LoginController@destroy')->name('logout');
});


Route::get('/home', 'HomeController@index')->name('home');
Route::get('users','UserController@getInfos');
Route::post('users','UserController@postInfos');

/**Route::get('drivers','DriverController@getInfos');
Route::post('drivers','DriverController@postInfos');
Auth::routes();

*/
